$(document).ready(function () {
    $("#changeButton").click(function () {
        $("h1, h2, h3, p").toggleClass("dark");
        $("html, .con1").toggleClass("background-dark");
        $(".isi").toggleClass("gelap");
    });

    var allPanels = $('#accordion > #isi').hide();

    $('#accordion > h3 > a').click(function () {
        if ($(this).parent().next().is(":hidden")){
            allPanels.slideUp();
            $(this).parent().next().slideDown();
            return false;
        } else {
            allPanels.slideUp();
        }
    });
});