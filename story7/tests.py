import os
from django.test import TestCase, Client
from django.urls import resolve
from .views import landing

class Story7UnitTest(TestCase):
    def test_url_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)
    def test_method_view(self):
        found = resolve('/')
        self.assertEqual(found.func, landing)
    def test_template_used(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'landing.html')
    def test_template_sesuai(self):
        response = Client().get('/')
        self.assertContains(response, "Hello !")
        self.assertContains(response, "Tentang Saya")
        # self.assertIn('name="button">Change Theme</button>', response.content.decode('utf8'))
        self.assertIn("><a>Daftar Aktivitas</a></h3>", response.content.decode())
        # self.assertIn('<div class="isi" id="isi">', response.content.decode())

import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from story7.settings import BASE_DIR
import time


class VisitorTest(unittest.TestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')        
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(executable_path=os.path.join(BASE_DIR,'chromedriver'), chrome_options=chrome_options)

    def tearDown(self):
        self.browser.quit()
    
    def test_button_change_theme(self):
        driver = self.browser
        driver.get('http://localhost:8000/')
        button = driver.find_element_by_name("button")
        button.click()
        body = driver.find_element_by_class_name("con1")
        tulisan = driver.find_element_by_name("isi-tulisan")
        assert "dark" in tulisan.get_attribute("class")
        assert "background-dark" in body.get_attribute("class")
    
    def test_accordion_is_running(self):
        driver = self.browser
        driver.get('http://localhost:8000/')
        section = driver.find_element_by_id("sek")
        section.click()
        tulisan = driver.find_element_by_name("isi-tulisan")
        assert "Akademis, Compfest, Wisgan," in driver.page_source
